\section{Non-dominated Sorting Genetic Algorithm II (NSGA-II)}
\hfill Jakub Poliński\\

NSGA-II jest zmodyfikowaną wersją popularnego algorytmu NSGA do optymalizacji wielokryterialnej. Podstawowy algorytm, choć charakteryzował się dobrymi rezultatami, był często krytykowany za swoją złożoność obliczeniową, brak elitaryzmu (zachowywania podpopulacji najlepszych osobników w kolejnych iteracjach algorytmu) oraz konieczność ustalania dodatkowego parametru a priori. NSGA-II został zaprojektowany, by wyeliminować problemy swojego poprzednika - twórcy algorytmu zastosowali lepszy algorytm sortujący oraz zmodyfikowali proces rozwoju populacji tak, by zapewnić elitaryzm oraz wyeliminować konieczność definiowania dodatkowych parametrów.

\subsection{Ogólny opis algorytmu}

Po zainicjowaniu populacji każdy osobnik jest przypisywany do odpowiedniego frontu na podstawie kryterium dominacji. Pierwszy front stanowią osobniki niezdominowane przez inne osobniki w populacji, drugi front stanowią osobniki, które są zdominowane wyłącznie przez osobniki z pierwszego frontu, na trzeci front składają się osobniki zdominowane przez dwa pierwsze fronty, itd. Każdemu osobnikowi zostaje przypisana wartość fitness na podstawie frontu, do którego należy (może to być np. numer frontu) oraz dodatkowy parametr, tzw. crowding distance, który jest miarą odległości osobnika w stosunku do jego sąsiadów. Następnie poprzez porównanie rangi (im mniejsza, tym lepszy osobnik) oraz miary odległości w przypadku remisu (im większa, tym lepszy osobnik) wybierana jest określona ilość najlepszych osobników w populacji, z których poprzez rekombinację oraz mutację powstaną nowe osobniki. Nowa populacja, złożona z dotychczasowych osobników oraz ich potomstwa, jest ponownie sortowana i okrajana do N najlepszych osobników, gdzie N jest przyjętym rozmiarem populacji. Cały proces od momentu podziału populacji na fronty do wyboru nowej powtarza się przez określoną ilość iteracji.

\subsection{Szczegóły algorytmu}

\subsubsection{Inicjalizacja populacji}

Pierwszą populację stanowią losowo wygenerowane osobniki, zachowujące narzucone ograniczenia liczbowe.

\subsubsection{Wyznaczanie frontów}

Poniżej przedstawiono opis jednego z możliwych algorytmów wyznaczających fronty na podstawie kryterium dominacji:

\begin{enumerate}
\item Dla każdego osobnika p w populacji P zrób:
\begin{itemize}
\item Przypisz $S_p = \emptyset$ oraz $n_p = 0$. Będą to kolejno: zbiór osobników zdominowanych przez p oraz ilość osobników, które dominują nad p.
\item Dla każdego osobnika q w P:
\begin{itemize}
\item Jeśli p dominuje nad q, dodaj q do $S_p$.
\item Jeśli q dominuje nad p, zwiększ $n_p$ o 1.
\end{itemize}
\item Jeśli $n_p$ jest równe 0, ustaw rangę osobnika $p_{rank} = 1$ oraz dodaj go do pierwszego frontu $F_1$.
\end{itemize}
\item Zainicjalizuj licznik frontów $i = 1$.
\item Dopóki i-ty front nie jest pusty, wykonuj:
\begin{itemize}
\item Przypisz $Q = \emptyset$ - w tym zbiorze będą przechowywane osobniki z frontu o numerze (i+1).
\item Dla każdego osobnika p z frontu $F_i$ oraz dla każdego osobnika q z $S_p$ zrób:
\begin{itemize}
\item Ustaw $n_q = n_q - 1$.
\item Jeśli $n_q = 0$, przypisz osobnikowi q rangę i+1 oraz dodaj go zbioru Q.
\end{itemize}
\item Ustaw $i = i + 1$.
\item Przypisz $F_i = Q$.
\end{itemize}
\end{enumerate}

\subsubsection{Wyznaczanie crowding distance}

W przypadku osobników znajdujących się w tym samym froncie, selekcja jest dokonywana na podstawie parametru crowding distance, określającego odległość euklidesową osobnika od jego sąsiadów w m-wymiarowej przestrzeni, gdzie m to liczba optymalizowanych funkcji. Procedura obliczania tej odległości jest następująca:

\begin{enumerate}
\item Dla każdego frontu $F_i$ złożonego z $n_i$ osobników zrób:
\begin{itemize}
\item Każdemu osobnikowi przypisz odległość $F_i(d_j) = 0$.
\item Dla każdej funkcji celu $f_m$ zrób:
\begin{itemize}
\item Posortuj osobniki z frontu $F_i$ względem celu m, tj. $I = sort(F_i, m)$.
\item Przypisz skrajnym osobnikom $I(d_1) = I(d_{n_i}) = \infty$.
\item Pozostałym osobnikom przypisz $I(d_k) = I(d_k) + f_m(d_{k+1}) + f_m(d_{k-1})$, gdzie $2 \leq k < n_i$.
\end{itemize}
\end{itemize}
\end{enumerate}

\subsubsection{Selekcja}

Selekcja osobników jest przeprowadzana metodą turniejową. Do porównania wykorzystywany jest operator $\prec_n$, zdefiniowany następująco:

\begin{itemize}
\item $p \prec_n q$ jeśli:
\begin{itemize}
\item $p_{rank} < q_{rank}$, tj. p dominuje nad q.
\item $p_{rank} = q_{rank}$ i $F_i(d_p) > F_i(d_q)$, tj. p i q są w tym samym froncie oraz p ma większy crowding distance niż q.
\end{itemize}
\end{itemize}

\subsubsection{Rekombinacja}

Nowy osobnik r, który powstaje z rodziców p oraz q, otrzymuje następujący zestaw cech, będący wektorem w przestrzeni rozwiązań o wymiarze n:

$r.attr[i] = \sqrt[2]{sgn(p.attr[i]*q.attr[i]) * p.attr[i]*q.attr[i]}, 1 \leq i \leq n$, gdzie sgn to funkcja signum.

\subsubsection{Mutacja}

Mutacja następuje bezpośrednio po rekombinacji z prawdopodobieństwem 0.1. Osobnik poddany mutacji uzyskuje losowo nowy zestaw cech, gdzie każda cecha jest wyznaczana losowo zgodnie z rozkładem normalnym o średniej równej dotychczasowej wartości cechy oraz odchyleniu standardowym równym $\frac{1}{10}$ zakresu przedziału, do którego ograniczona jest każda cecha.