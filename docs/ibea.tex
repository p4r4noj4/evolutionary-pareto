\section{Indicator-Based Evolutionary Algorithm (IBEA)}
\hfill Radosław Łazarz\\

IBEA jest nietypowym przedstawicielem rodziny algorytmów MOEA (MultiObjective Evolutionary Algorithms). Opiera się on na idei sformalizowania preferencji w postaci ciągłych uogólnień relacji dominacji. Pozwala to nie tylko na adaptację z uwzględnieniem dowolnych preferencji czy optymalizacji, ale również na poprawną pracę algorytmu bez konieczności korzystania z dodatkowych metod zachowania różnorodności populacji. W pracy skoncentrowano się na drugim z wymienionych aspektów. W tym celu zaimplementowano algorytm w wariancie AIBEA (Adaptive IBEA). Założono również, iż funkcje celu należy minimalizować.

\subsection{Szkic algorytmu}

\begin{algorithm}[H]
\KwIn{$\alpha$ (rozmiar populacji podstawowej), $\alpha'$ (rozmiar populacji rozpłodowej), $N$ (maksymalna ilość kroków), $\kappa$ (czynnik skalujący)}
\KwOut{$A$ (przybliżenie zbioru Pareto)}
inicjuj populację podstawową\;
\While{ilość kroków $< N$}{
  oblicz wskaźniki przystosowania\;
  \While{rozmiar populacji $> \alpha$}{
    dokonaj selekcji środowiskowej\;
  }
  \If{dodatkowy warunek kończący} {
    zakończ\;
  }
  dokonaj selekcji populacji rozpłodowej\;
  dokonaj operacji crossing-over\;
  dokonaj mutacji\;
  dołącz populację rozpłodową do początkowej\;
  ilość kroków += 1\;
}
\caption{Adaptive IBEA}
\end{algorithm}

\subsubsection{Inicjacja populacji początkowej}

Populacja początkowa $P$ to zbiór $\alpha$ wektorów z przestrzeni poszukiwań, losowanych z rozkładem równomiernym (uniform). Oznaczmy je przez $\mathbf{x} \in \mathbb{R}^d$, gdzie $d$ to ilość wymiarów przestrzeni poszukiwań.

\subsubsection{Obliczanie wskaźników przystosowania}

Przystosowanie obliczamy dla znormalizowanych funkcji celu. By je uzyskać, dla każdej z pierwotnych funkcji celu $f_i$ znajdujemy minimalną i maksymalną wartości $f_i(\mathbf{x}) : \mathbf{x} \in P$. Oznaczamy je odpowiednio $min_{f_i}$ i $max_{f_i}$. Następnie definiujemy znormalizowaną funkcję celu jako
$$
f_{i}'(\mathbf{x}) = (f_{i}(\mathbf{x}) - min_{f_i})/(max_{f_i} - min_{f_i}).
$$

Następnie obliczamy wartości indykatorów jakości $I(\mathbf{x_1},\mathbf{x_2})$ dla każdej pary $\mathbf{x_1}, \mathbf{x_2} \in P$ (wykorzystując znormalizowane funkcje celu $f_{i}'$. Obliczamy również współczynnik skalowania indykatorów:
$$
c = max(|I(\mathbf{x_1},\mathbf{x_2})|).
$$

Wskaźnik przystosowania dla każdego osobnika populacji wyznacza następujący wzór:
$$
F(\mathbf{x_1}) = \sum_{\mathbf{x_2} \in P \setminus \{\mathbf{x_1}\}} -e^{-\frac{I(\mathbf{x_2},\mathbf{x_1})}{c \kappa}} .
$$
Tak zdefiniowana funkcja przystosowania pozwala na modyfikację działania algorytmu poprzez dobór właściwych indykatorów (np. promujących osobniki unikatowe w celu zwiększenia różnorodności populacji).

\subsubsection{Selekcja środowiskowa}

Istotą selekcji środowiskowej jest usunięcie z populacji osobnika o najniższym przystosowaniu, oznaczanego odtąd $\mathbf{x_*}$. Po wykonaniu tej operacji konieczna jest aktualizacja wartości funkcji przystosowania pozostałych osobników.
$$
F_{nowa}(\mathbf{x}) = F_{stara}(\mathbf{x}) + e^{-\frac{I(\mathbf{x_*},\mathbf{x})}{c \kappa}}
$$

\subsubsection{Selekcja populacji rozpłodowej}

Populacja rozpłodowa $P'$ jest generowana na bazie populacji $P$ poprzez binarny turniej ze zwracaniem. Z $P$ losowane są dwa (niekoniecznie różne) osobniki $\mathbf{x_1}$ i $\mathbf{x_1}$. Jeżeli $F(\mathbf{x_1}) > F(\mathbf{x_2})$ to do $P'$ osobnik $\mathbf{x_1}$ wchodzi z prawdopodobieństwem $p=0.95$, a osobnik $\mathbf{x_2}$ z $p=0.05$, w przeciwnym zaś wypadku ma miejsce sytuacja odwrotna. Operację tę powtarzamy $2\alpha'$ razy.

\subsubsection{Crossing-over i mutacja}

Operacja rekombinacji (crossing-over) jest wykonywana na każdej kolejnej parze osobników z $P'$. Jej skutkiem jest wytworzenie jednego osobnika potomnego, który powstaje poprzez wybranie w losowy sposób (z równym prawdopodobieństwem) wartości każdej z cech jako kopii cechy jednego z osobników--rodziców. Za cechę uznajemy każdą ze współrzędnych wektora w przestrzeni przeszukiwań, liczba cech jest więc równa ilości wymiarów tej przestrzeni. Po zakończeniu tej operacji moc zbioru $P'$ zmniejsza się o połowę i wynosi $\alpha'$.

Mutacja zachodzi dla każdej cechy każdego osobnika z populacji rozpłodowej po zakończeniu rekombinacji z prawdopodobieństwem $p=0.05$. Polega na zamianie wartości danej cechy na wartość losową pochodzącą z rozkładu normalnego o wartości średniej równej dawnej wartości cechy i odchyleniu standardowym zależnym od rzędu wielkości wartości opisujących daną cechę (w praktyce równym w przybliżeniu 0.3 dla benchmarkowanych cech).

\subsection{Indykatory jakości}

W najogólniejszym ujęciu indykator jest funkcją przyjmującą jako argumenty $k$ przybliżeń zbioru Pareto i zwracającą liczbę rzeczywistą. Najpopularniejsze są indykatory unarne ($k=1$). W przypadku opisywanego algorytmu wykorzystano jednakże indykatory binarne ($k=2$), których wartość można interpretować jako ``jakiej jakości jest przybliżenie $A$ w porównaniu z przybliżeniem $B$''. Jakość może być rozumiana na wiele sposobów (który zbiór lepiej przybliża rzeczywisty zbiór Pareto, czy zbiory są istotnie różne, etc.), kontrolując w ten sposób działanie algorytmu. Dodatkowo, właściwie zdefiniowane indykatory binarne mogą być naturalnym rozszerzeniem relacji dominacji w sensie Pareto i używane zamiast niej w popularnych metodach wyznaczania stopnia przystosowania. Warto również zauważyć, że choć obliczanie wartości indykatorów binarnych może być kosztowne, to w omawianym przypadku oceniane przybliżenia zbioru Pareto są zawsze jednoelementowe, co istotnie redukuje złożoność obliczeń.

\subsubsection{Indykator $I_{\epsilon^{+}}$}

Indykator użyty w testowanej implementacji. Jego formalna definicja wyraża się następującym wzorem:
$$
I_{\epsilon^{+}}(A, B) = min_{\epsilon}\{ \forall \mathbf{x_2} \in B\; \exists \mathbf{x_1} \in A : f_i(\mathbf{x_1}) - \epsilon \leq f_i(\mathbf{x_2}),\: i \in \{1,\dotsc,n\}\},
$$
gdzie $n$ to ilość funkcji celu, a $A$ i $B$ to przybliżenia zbioru Pareto. Należy zwrócić uwagę, iż indykator ten w połączeniu z algorytmem IBEA pozwala na automatyczne zapewnienie różnorodności populacji. Jeżeli wystąpi w niej osobnik, który nie jest dominowany przez pozostałe, ale jest od nich istotnie różny uzyska on wyższe przystosowanie, niż osobniki z grupy wzajemnie podobnych.
