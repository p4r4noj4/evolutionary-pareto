LaTeX
=====

Przygotowanie środowiska, informacje ogólne
-------------------------------------------

- LNCS guidelines: [Springer-Guidelines]_ (mirror: [Springer-Guidelines]_).
- Instalacja nowego TeXLive na Ubuntu: [TeXLive-Ubuntu]_.
- Instalacja LNCS: [LNCS-Ubuntu]_.

.. [Springer-Guidelines]: http://static.springer.com/sgw/documents/1121537/application/pdf/Springer_CS_Proceedings_Author_Guidelines_Jan_2013.pdf
.. [Springer-Guidelines-Mirror]: https://www.dropbox.com/s/99aseoozycpmld0/Springer_CS_Proceedings_Author_Guidelines_Jan_2013.pdf
.. [LNCS-Ubuntu]: https://jfdm.wordpress.com/2009/09/01/quickquide-adding-lncs-springer-style-to-ubuntu-latex-install/
.. [TeXLive-Ubuntu]: http://askubuntu.com/questions/163682/how-do-i-install-the-latest-tex-live-2012

Konferencja
===========

Chyba nie będziemy musieli tego podpisywać, ale czort wie: [LNCS-Springer-Copyright]_ (mirror: [LNCS-Springer-Copyright-Mirror]_).

.. [LNCS-Springer-Copyright]: ftp://ftp.springer.de/pub/tex/latex/llncs/LNCS-Springer_Copyright_Form.pdf
.. [LNCS-Springer-Copyright-Mirror]: https://www.dropbox.com/s/ft5k5g4hkhl6s12/LNCS-Springer_Copyright_Form.pdf
