import unittest

#noinspection PyPep8Naming
import itertools
import ep.hgs.hgs as hgs
import ep.spea2.spea2 as spea2
from ep.utils import ea_utils
from problems.kursawe import problem

from problems.testrun import TestRun


#
#
# PyCharm Unittest runner setting: working directory set to Git-root (`evolutionary-pareto` dir).
#
#

#noinspection PyPep8Naming
class TestRunHGSwithSPEA2(TestRun):
    alg_name = "hgs_spea2"

    @TestRun.skipByName()
    @TestRun.map_param('budget', range(4000, 10000, 1000),
                       gather_function=TestRun.gather_function)
    def test_quick(self, budget=None):

        init_population = ea_utils.gen_population(100, problem.dims)
        sclng_coeffs = [[4, 4, 4], [2, 2, 2], [1, 1, 1]]
        self.alg = hgs.HGS.make_std(dims=problem.dims,
                                    population=init_population,
                                    fitnesses=problem.fitnesses,
                                    popln_sizes=[len(init_population), 10, 5],
                                    sclng_coeffss=sclng_coeffs,
                                    muttn_varss=hgs.HGS.make_sigmas(20, sclng_coeffs, problem.dims),
                                    csovr_varss=hgs.HGS.make_sigmas(10, sclng_coeffs, problem.dims),
                                    sprtn_varss=hgs.HGS.make_sigmas(100, sclng_coeffs, problem.dims),
                                    brnch_comps=[0.05, 0.25, 0.01],
                                    metaepoch_len=1,
                                    max_children=2,
                                    driver=spea2.SPEA2,
                                    stop_conditions=[])

        self.run_alg(budget, problem)

    @TestRun.skipByName()
    def test_heavy_wnioski1_lol(self):
        self.hgs_runner(muttn_vars=[1.6, 0.2, 0.4], popln_size=[100, 50, 15], brnch_comps=[None, 0.4, 0.5])

    @TestRun.skipByName()
    def test_heavy_wnioski1_A(self):
        self.hgs_runner(muttn_vars=[1.6, 0.2, 0.4], popln_size=[100, 50, 15], brnch_comps=[None, 0.4, 0.5],
                        metaepoch_len=40, stps_cnt=10)

    @TestRun.skipByName()
    def test_heavy_wnioski1_B(self):
        self.hgs_runner(muttn_vars=[1.6, 0.2, 0.4], popln_size=[75, 20, 5], brnch_comps=[None, 0.4, 0.5], )

    @TestRun.skipByName()
    def test_normal_normal_domyslne(self):
        self.hgs_runner()

    @TestRun.skipByName()
    def test_normal_Mut1a(self):
        self.hgs_runner(muttn_vars=[0.8, 0.4, 0.2])

    @TestRun.skipByName()
    def test_normal_Mut1b(self):
        self.hgs_runner(muttn_vars=[2.4, 0.4, 0.2])

    @TestRun.skipByName()
    def test_normal_Mut2a(self):
        self.hgs_runner(muttn_vars=[1.6, 0.2, 0.2])

    @TestRun.skipByName()
    def test_normal_Mut2b(self):
        self.hgs_runner(muttn_vars=[1.6, 0.4, 0.2])

    @TestRun.skipByName()
    def test_normal_Mut3a(self):
        self.hgs_runner(muttn_vars=[1.6, 0.4, 0.1])

    @TestRun.skipByName()
    def test_normal_Mut3b(self):
        self.hgs_runner(muttn_vars=[1.6, 0.4, 0.4])

    @TestRun.skipByName()
    def test_normal_PopA(self):
        self.hgs_runner(popln_size=[100, 20, 5])

    @TestRun.skipByName()
    def test_normal_PopB(self):
        self.hgs_runner(popln_size=[100, 50, 5])

    @TestRun.skipByName()
    def test_normal_PopC(self):
        self.hgs_runner(popln_size=[100, 50, 15])

    @TestRun.skipByName()
    def test_normal_MetLenA(self):
        self.hgs_runner(metaepoch_len=10)  # **

    @TestRun.skipByName()
    def test_normal_MetLenB(self):
        self.hgs_runner(metaepoch_len=30)

    @TestRun.skipByName()
    def test_normal_MetLenC(self):
        self.hgs_runner(metaepoch_len=40)

    @TestRun.skipByName()
    def test_normal_BrnCmpA(self):
        self.hgs_runner(brnch_comps=[None, 0.1, 0.5])

    @TestRun.skipByName()
    def test_normal_BrnCmpB(self):
        self.hgs_runner(brnch_comps=[None, 0.3, 0.5])

    @TestRun.skipByName()
    def test_normal_BrnCmpC(self):
        self.hgs_runner(brnch_comps=[None, 0.4, 0.5])  # **

    @TestRun.skipByName()
    def test_normal_BrnCmpD(self):
        self.hgs_runner(brnch_comps=[None, 0.5, 0.5])

    @TestRun.skipByName()
    def test_normal_BrnCmpE(self):
        self.hgs_runner(brnch_comps=[None, 0.1, 0.25])

    @TestRun.skipByName()
    def test_normal_wnioski2A(self):
        self.hgs_runner(metaepoch_len=10, brnch_comps=[None, 0.4, 0.5], max_children=3, sproutiveness=1)

    @TestRun.skipByName()
    def test_normal_wnioski2B(self):
        self.hgs_runner(metaepoch_len=10, brnch_comps=[None, 0.4, 0.5], max_children=3, sproutiveness=1,
                        stps_cnt=3)

    @TestRun.skipByName()
    def test_normal_wnioski2C(self):
        self.hgs_runner(metaepoch_len=10, brnch_comps=[None, 0.4, 0.5], max_children=3, sproutiveness=1,
                        stps_cnt=2)

    @TestRun.skipByName()
    def test_normal_BrnCmpF(self):
        self.hgs_runner(brnch_comps=[None, 0.3, 0.125])

    @TestRun.skipByName()
    def test_normal_BrnCmpG(self):
        self.hgs_runner(brnch_comps=[None, 0.4, 0.75])

    @TestRun.skipByName()
    def test_normal_BrnCmpH(self):
        self.hgs_runner(brnch_comps=[None, 0.5, 1.0])

    @TestRun.skipByName()
    def test_normal_ChldrnA(self):
        self.hgs_runner(max_children=3, sproutiveness=1)  # **

    @TestRun.skipByName()
    def test_normal_ChldrnB(self):
        self.hgs_runner(max_children=4, sproutiveness=2)

    @TestRun.skipByName()
    def test_normal_ChldrnC(self):
        self.hgs_runner(max_children=5, sproutiveness=2)

    @TestRun.skipByName()
    def test_normal_ChldrnD(self):
        self.hgs_runner(max_children=5, sproutiveness=3)

    #noinspection PyDefaultArgument
    def hgs_runner(self,
                   sclng_coeffs=[[25, 25, 25], [4, 4, 4], [1, 1, 1]],
                   muttn_vars=[1.6, 0.4, 0.2], csovr_vars=[0.4, 0.4, 0.4], sprtn_vars=[0.125, 0.125, 0.125],
                   brnch_comps=[None, 0.2, 0.5], popln_size=[70, 20, 5],
                   metaepoch_len=20, max_children=5, stps_cnt=4, sproutiveness=3):
        init_population = ea_utils.gen_population(popln_size[0], problem.dims)
        self.alg = hgs.HGS.make_std(dims=problem.dims,
                                    population=init_population,
                                    fitnesses=problem.fitnesses,
                                    popln_sizes=popln_size,
                                    sclng_coeffss=sclng_coeffs,
                                    muttn_varss=[[v, v, v] for v in muttn_vars],
                                    csovr_varss=[[v, v, v] for v in csovr_vars],
                                    sprtn_varss=[[v, v, v] for v in sprtn_vars],
                                    brnch_comps=brnch_comps,
                                    metaepoch_len=metaepoch_len,
                                    max_children=max_children,
                                    driver=spea2.SPEA2,
                                    stop_conditions=[],
                                    sproutiveness=sproutiveness)

        self.cost = self.alg.steps(self.printing_range(stps_cnt))
        self.problem_mod = problem
        self.result = self.alg.population


if __name__ == '__main__':
    unittest.main()
