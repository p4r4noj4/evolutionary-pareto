import unittest

#noinspection PyPep8Naming
import itertools
import ep.spea2.spea2 as spea2
from ep.utils import ea_utils
from problems.kursawe import problem

from problems.testrun import TestRun


#
#
# PyCharm Unittest runner setting: working directory set to Git-root (`evolutionary-pareto` dir).
#
#


#noinspection PyPep8Naming
class TestRunSPEA2(TestRun):
    alg_name = "spea2"

    @TestRun.skipByName()
    def test_quick(self):
        var = [abs(maxa-mina)/100
               for (mina, maxa) in problem.dims]
        self._run_test(var, 75, 3)


    @TestRun.skipByName()
    def test_normal_Mut1a(self):
        self._run_test([0.8, 0.4, 0.2])

    @TestRun.skipByName()
    def test_normal_Mut1b(self):
        self._run_test([2.4, 0.4, 0.2])

    @TestRun.skipByName()
    def test_normal_Mut2a(self):
        self._run_test([1.6, 0.2, 0.2])

    @TestRun.skipByName()
    def test_normal_Mut2b(self):
        self._run_test([1.6, 0.4, 0.2])

    @TestRun.skipByName()
    def test_normal_Mut3a(self):
        self._run_test([1.6, 0.4, 0.1])

    @TestRun.skipByName()
    def test_normal_Mut3b(self):
        self._run_test([1.6, 0.4, 0.4])

    @TestRun.skipByName()
    @TestRun.map_param('budget', range(4000, 10000, 1000),
                       gather_function=TestRun.gather_function)
    def test_final(self, budget=None):
        self._run_test([0.8, 0.4, 0.2], 100, budget, itertools.count())


    def _run_test(self, var, popsize=100, budget=None, steps_gen=range(20)):
        init_population = ea_utils.gen_population(popsize, problem.dims)

        self.alg = spea2.SPEA2(dims=problem.dims,
                               population=init_population,
                               fitnesses=problem.fitnesses,
                               mutation_variance=var,
                               crossover_variance=var)
        self.run_alg(budget, problem, steps_gen)

if __name__ == '__main__':
    unittest.main()
