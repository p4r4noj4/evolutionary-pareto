########################################################################################################################
#
#
#  ______     ________   _______    _______      ________     ______        _        _________   ________   ______
# |_   _ `.  |_   __  | |_   __ \  |_   __ \    |_   __  |  .' ___  |      / \      |  _   _  | |_   __  | |_   _ `.
#   | | `. \   | |_ \_|   | |__) |   | |__) |     | |_ \_| / .'   \_|     / _ \     |_/ | | \_|   | |_ \_|   | | `. \
#   | |  | |   |  _| _    |  ___/    |  __ /      |  _| _  | |           / ___ \        | |       |  _| _    | |  | |
#  _| |_.' /  _| |__/ |  _| |_      _| |  \ \_   _| |__/ | \ `.___.'\  _/ /   \ \_     _| |_     _| |__/ |  _| |_.' /
# |______.'  |________| |_____|    |____| |___| |________|  `.____ .' |____| |____|   |_____|   |________| |______.'
#
#
#
# Nie, serio.
#
# Cokolwiek zmieniacie, zmieniajcie od razu w problems/**/run.py .
#
#
#
#
########################################################################################################################
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
__author__ = 'Prpht'

import math

#PARABOL
parabol = ([
               lambda x: (x[0]-3)**2,
            lambda x: (x[1]+2)**2],
           [(-10, 10), (-10, 10)], 'parabol', [[0], [0]])

#KURSAWE
with open('kursawe.dat', 'r') as f:
    lines = f.readlines()
kursawe_anal = [[float(line[:-1].split('\t')[0]) for line in lines], [float(line[:-1].split('\t')[1]) for line in lines]]

kursawe = ([lambda x : (-10 * math.exp(-0.2 * math.sqrt(x[0]*x[0]+x[1]*x[1])) + -10 * math.exp(-0.2 * math.sqrt(x[1]*x[1]+x[2]*x[2]))),
            lambda x : math.pow(abs(x[0]),0.8) + 5*math.pow(math.sin(x[0]),3) + math.pow(abs(x[1]),0.8) + 5*math.pow(math.sin(x[1]),3) + math.pow(abs(x[2]),0.8) + 5*math.pow(math.sin(x[2]),3)],
           [(-5, 5), (-5, 5), (-5, 5)], 'kursawe', kursawe_anal)

#EMOA
def emoa_fitnesses(f1, g, h, dims, letter, anal):
    return ([lambda x : f1(x[0]), lambda x : emoa_fitness_2(f1, g, h, x)],
            [(0, 1) for _ in range(dims)], "coemoa_" + letter, anal)

def emoa_fitnesses2(f1, g, h, dims, letter, anal):
    return ([lambda x : f1(x[0]), lambda x : emoa_fitness_2(f1, g, h, x)],
            [(0, 1)] + [(-5, 5) for _ in range(dims-1)], "coemoa_" + letter, anal)

p_no = 150
emoa_points = [i/(p_no-1) for i in range(p_no)]
p_no2 = 300
emoa_points2 = [i/(p_no2-1) for i in range(p_no2)]

def emoa_fitness_2(f1, g, h, x):
    y = g(x[1:])
    return y * h(f1(x[0]), y)

def f1a(x):
    return x

def ga(xs):
    return 1 + 0.3103448275862069 * sum(xs)  # 9 / (30-1) = 0.31...

def ha(f1, g):
    return 1 - math.sqrt(abs(f1 / g))

emoa_a_analytical = [[x for x in emoa_points], [1-math.sqrt(x) for x in emoa_points]]
emoa_a = emoa_fitnesses(f1a, ga, ha, 30, 'a', emoa_a_analytical)


def f1b(x):
    return x

def gb(xs):
    return 1 + 0.3103448275862069 * sum(xs)  # 9 / (30-1) = 0.31...

def hb(f1, g):
    return 1 - (f1 / g) ** 2

emoa_b_analytical = [[x for x in emoa_points], [1-x*x for x in emoa_points]]
emoa_b = emoa_fitnesses(f1b, gb, hb, 30, 'b', emoa_b_analytical)

def f1c(x):
    return x

def gc(xs):
    return 1 + 0.3103448275862069 * sum(xs)  # 9 / (30-1) = 0.31...

def hc(f1, g):
    f1_g = f1 / g
    return 1 - math.sqrt(abs(f1_g)) - f1_g * math.sin(10 * math.pi * f1)

c_pre = [[x for x in emoa_points2], [1-math.sqrt(x)-x*math.sin(10*math.pi*x) for x in emoa_points2]]
to_remove = [False] + [c_pre[1][i] >= c_pre[1][i-1] for i in range(1, len(c_pre[1]))]
#print(to_remove)
emoa_c_analytical = [[c_pre[0][i] for i in range(len(c_pre[1])) if not to_remove[i]], [c_pre[1][i] for i in range(len(c_pre[1])) if not to_remove[i]]]
emoa_c = emoa_fitnesses(f1c, gc, hc, 30, 'c', emoa_c_analytical)

fpi = 4 * math.pi

def f1d(x):
    return x

def gd(xs):
    return 91 + sum(x ** 2 - 10 * math.cos(fpi * x) for x in xs)

def hd(f1, g):
    return 1 - math.sqrt(abs(f1 / g))

emoa_d_analytical = [[x for x in emoa_points], [1-math.sqrt(x) for x in emoa_points]]
emoa_d = emoa_fitnesses2(f1d, gd, hd, 10, 'd', emoa_d_analytical)

spi = 6 * math.pi

def f1e(x):
    return 1 - math.exp(-4 * x) * (math.sin(spi * x) ** 6)

def ge(xs):
    return 1 + 5.19615 * sum(xs) ** 0.25

def he(f1, g):
    return 1 - (f1 / g) ** 2

emoa_e_analytical = [[f1e(x) for x in emoa_points], [1-f1e(x)*f1e(x) for x in emoa_points]]
emoa_e = emoa_fitnesses(f1e, ge, he, 10, 'e', emoa_e_analytical)