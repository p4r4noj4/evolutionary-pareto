########################################################################################################################
#
#
#  ______     ________   _______    _______      ________     ______        _        _________   ________   ______
# |_   _ `.  |_   __  | |_   __ \  |_   __ \    |_   __  |  .' ___  |      / \      |  _   _  | |_   __  | |_   _ `.
#   | | `. \   | |_ \_|   | |__) |   | |__) |     | |_ \_| / .'   \_|     / _ \     |_/ | | \_|   | |_ \_|   | | `. \
#   | |  | |   |  _| _    |  ___/    |  __ /      |  _| _  | |           / ___ \        | |       |  _| _    | |  | |
#  _| |_.' /  _| |__/ |  _| |_      _| |  \ \_   _| |__/ | \ `.___.'\  _/ /   \ \_     _| |_     _| |__/ |  _| |_.' /
# |______.'  |________| |_____|    |____| |___| |________|  `.____ .' |____| |____|   |_____|   |________| |______.'
#
#
#
# Nie, serio.
#
# Cokolwiek zmieniacie, zmieniajcie od razu w problems/**/run.py .
#
#
#
#
########################################################################################################################
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#

__author__ = 'Prpht'

from benchmarks import parabol, kursawe, emoa_a, emoa_b, emoa_c, emoa_d, emoa_e
from ep.utils import ea_utils
from ep.hgs import hgs
from ep.ibea import ibea
from ep.nsga2 import nsga2
from ep.spea2 import spea2
import pylab
import random
import time

random.seed()

benchmarks = [parabol, kursawe, emoa_a, emoa_b, emoa_c, emoa_d, emoa_e]

def save_plot(fitnesses, population, name, algo, analytical, time):
    print("   Finished for algorithm: " + algo)
    X = [fitnesses[0](x) for x in population]
    Y = [fitnesses[1](x) for x in population]
    fig = pylab.figure()
    pylab.plot()
    pylab.xlabel("funkcja celu I")
    pylab.ylabel("funkcja celu II")
    pylab.figtext(.50, .95, "czas wykonywania: " + str(time) + " s", horizontalalignment='center', verticalalignment='center')
    pylab.scatter(X,Y, c='b')
    pylab.scatter(analytical[0], analytical[1], c='r')
    fig.savefig('docs/figures/' + name + '_' + algo + '.png')
    # pylab.show()

for fitnesses, dims, name, anal in benchmarks:
    print("Starting benchmark: " + name)
    var = [abs(maxa-mina)/100 for (mina, maxa) in dims]
    #IBEA
    start = time.clock()
    initial_population = ea_utils.gen_population(80, dims)
    kappa = 0.05
    mating_size = 0.5
    ibea_population = ibea.IBEA(1, dims, fitnesses, var, var, kappa, mating_size)
    ibea_population.population = initial_population
    ibea_population.step(500)
    ibea_result = ibea_population.finish()
    elapsed = (time.clock() - start)
    save_plot(fitnesses, ibea_result, name, 'ibea', anal, elapsed)
    #NSGA2
    start = time.clock()
    initial_population = ea_utils.gen_population(80, dims)
    mating_size = 0.5
    nsga2_population = nsga2.NSGA2(dims, fitnesses, var, var, mating_size)
    nsga2_population.population = initial_population
    nsga2_population.step(1500)
    nsga2_result = nsga2_population.finish()
    elapsed = (time.clock() - start)
    save_plot(fitnesses, nsga2_result, name, 'nsga2', anal, elapsed)
    ##SPEA2
    #start = time.clock()
    #search_space = [range(mina, maxa) for mina, maxa in dims]
    #max_gens = 100
    #pop_size = 200
    #archive_size = 100
    #p_cross = 0.9
    #spea2_old.objective_one = fitnesses[0]
    #spea2_old.objective_two = fitnesses[1]
    #spea2_archive = spea2_old.search(search_space, max_gens, pop_size, archive_size, p_cross)
    #spea2_result = [x['vector'] for x in spea2_archive]
    #elapsed = (time.clock() - start)
    #save_plot(fitnesses, spea2_result, name, 'spea2', elapsed)
    #HGS + SGA
    # start = time.clock()
    # initial_population = utils.gen_population(1000, dims)
    # hgs_sga = hgs_mod.HGS(fitnesses=fitnesses,
    #                   population=initial_population,
    #                   domain_dim=dims,
    #                   driver_kwargs={},
    #                   driver=sga.SGA)
    # hgs_sga.run()
    # elapsed = (time.clock() - start)
    # save_plot(fitnesses, hgs_sga.population, name, 'hgs_sga', elapsed)
    #HGS + IBEA
    # start = time.clock()
    # initial_population = utils.gen_population(1000, dims)
    # hgs_ibea = hgs_mod.HGS(fitnesses=fitnesses,
    #                   population=initial_population,
    #                   domain_dim=dims,
    #                   driver_kwargs={'kappa' : 0.05, 'mating_population_size' : 10},
    #                   driver=ibea.IBEA)
    # hgs_ibea.run()
    # elapsed = (time.clock() - start)
    # save_plot(fitnesses, hgs_ibea.population, name, 'hgs_ibea', elapsed)